from isiguardianlib.isolation.const import ISOLATION_CONSTANTS
ISOLATION_CONSTANTS['ALL_DOF'] = ['X', 'Y', 'RZ', 'Z', 'RX', 'RY']
ISOLATION_CONSTANTS['DOF_LISTS'] = (['Z', 'RX', 'RY'], ['X', 'Y', 'RZ'])
ISOLATION_CONSTANTS['CART_BIAS_DOF_LISTS'] = ([], [])
ISOLATION_CONSTANTS['FF_DOF']['FF01'] = ['X', 'Y', 'Z']
from isiguardianlib.ISI_STAGE import *
prefix = 'ISI-ETMX_ST1'
nominal = 'HIGH_ISOLATED'
